function allListsThatBelongToaBoardBasedOnThboardID( data, boardID) {
  let promise = new Promise((resolve, reject)=>{
    setTimeout(() => {
      let list ;
      for (const key in data) {
          if (key === boardID) {
              list = data[key]
          }
      }
      resolve(list)
    }, 2* 1000);
  })
   return promise
}

module.exports =  allListsThatBelongToaBoardBasedOnThboardID
