function getInfoForThanosBoard(boardDetails, listDetails, cardDetails, boardData, listData, cardsData) {
    boardDetails(boardData, "mcu453ed")
    .then((boardDs) => {
      console.log(boardDs.id);
      return listDetails(listData, boardDs.id);
    })
    .then((listsForThanos) => {
      console.log(listsForThanos);
      let getIdOfmind = listsForThanos.find((data) => data.name === "Mind");
      return cardDetails(cardsData, getIdOfmind.id);
    })
    .then((cardsForMind) => {
      console.log(cardsForMind);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

module.exports = getInfoForThanosBoard;

