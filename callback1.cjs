function detailsFindOutById(data, id) {
  let result = new Promise((reslove, reject) => {
    setTimeout(() => {
      const boardDetails = data.find((board) => board.id == id);
      reslove(boardDetails);
    }, 2 * 1000);
  });
  return result;
}

module.exports =  detailsFindOutById
