function getInfoForThanosBoard(boardDetails, listDetails, cardDetails, boardData, listData, cardsData) {
  boardDetails(boardData, "mcu453ed")
   .then((boardDs) => {
     console.log(boardDs.id);
     return listDetails(listData, boardDs.id);
   })
   .then((listsForThanos) => {
     console.log(listsForThanos);
     const cardPromises = [];
     listsForThanos.forEach((element) => {
         cardPromises.push(cardDetails(cardsData, element.id))
     });
     return Promise.all(cardPromises);
   }).then((reslove)=>{
     console.log(reslove)
   })
   .catch((error) => {
     console.error("Error:", error);
   });
}

module.exports = getInfoForThanosBoard;
