function allCardsThatBelongToaParticularListBasedOnThelistID( data, listID) {
  let promise = new Promise((resolve, reject)=>{
    setTimeout(() => {
      let card ;
      for (const key in data) {
          if (key === listID) {
              card = data[key]
          }
      }
        resolve(card)
    }, 2 * 1000);
  })
   return promise
}

function callback(boardDetails) {
    console.log(boardDetails);
  }

  
module.exports = allCardsThatBelongToaParticularListBasedOnThelistID
